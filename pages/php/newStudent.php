<?php 
    include("connexion.php");
	if(isset($_POST['btnEnregistrer']))
	{
        $matricule =  addslashes(htmlentities(trim(strip_tags($_POST['matricule']))));
		$nom = addslashes(htmlentities(trim(strip_tags($_POST['nom']))));
		$prenom =  addslashes(htmlentities(trim(strip_tags($_POST['prenom']))));
		$datenaiss =  addslashes(htmlentities(trim(strip_tags($_POST['datenaiss']))));
		$lieunaiss =  addslashes(htmlentities(trim(strip_tags($_POST['lieunaiss']))));
		$sexe =  addslashes(htmlentities(trim(strip_tags($_POST['sexe']))));
 
        if($sexe != "M" && $sexe ="F"){
            $sexe = "";
        }

		if(empty($nom) || empty($prenom) || empty($datenaiss) || empty($lieunaiss) || empty($sexe))
		{
			?>
			<script type="text/javascript">
				alert("Veuillez remplir tous les champs SVP");
			</script>
			<?php
		}
		else
		{
			$sql = "INSERT INTO students (matricule, nom, prenom, datenaiss, lieunaiss, sexe) VALUES (:matricule, :nom, :prenom, :datenaiss, :lieunaiss, :sexe)";
			$sth = $connexion->prepare($sql);
			$sth->execute(array(
                ':matricule' => $matricule,
				':nom'=>$nom, 
				':prenom'=>$prenom, 
				':datenaiss'=>$datenaiss, 
				':lieunaiss'=>$lieunaiss, 
				':sexe'=>$sexe,
			));
			if($sth)
			{
				?>
				<script type="text/javascript">
					alert("Elève enregistré avec succès");
					location='../front/liste.php';
				</script>
				<?php
			}
			else
			{
				?>
				<script type="text/javascript">
					alert("Echec de l'enregistrement");
				</script>
				<?php
			}
		}
	}