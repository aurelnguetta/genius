<?php 
    require_once('../php/connexion.php');
    if(isset($_GET['matricule']) && !empty($_GET['matricule'])){
        $matricule = addslashes(htmlentities(trim(strip_tags($_GET['matricule']))));
    }else{
        header("Location:liste.php");
    }
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SCHOOL BOX</title>
    <link rel="stylesheet" href="../../css/form.css">
</head>
<body>
    <div class="back">
        <section>
            <nav class="menu">
                <ul>
                    <li><a href="../../index.php">AJOUTER ETUDIANT</a></li>
                    <li><a href="liste.php">LISTE DES ETUDIANTS</a></li>
                    <!-- <li class="active"><a href="#">MA CARTE ETUDIANT</a></li> -->
                </ul>
            </nav>
            <div class="liste" style="align-items: center !important;">
                <!-- <h3>LISTE DES ETUDIANTS</h3> -->
                <div class="cart">
                    <div class="cart-head">
                        <img src="../../images/cart_head.png" alt="">
                    </div>
                    <div class="cart-body">
                        <div class="profil">
                            <img src="../../images/user.png" alt="">
                        </div>
                        <div class="desc">
                        <?php
                            $sql = "SELECT * FROM students WHERE matricule='$matricule'";
                            $sth = $connexion->prepare($sql);
                            $sth->execute();
                            while($tab=$sth->fetch(PDO::FETCH_ASSOC))
                            {
                                ?>
                                    <span><?php echo $tab['matricule'] ?></span>
                                    <span><?php echo $tab['nom'] ?></span>
                                    <span><?php echo $tab['prenom'] ?></span>
                                    <span><?php echo $tab['datenaiss'] ?></span>
                                    <span><?php echo $tab['lieunaiss'] ?></span>
                                    <span><?php echo $tab['sexe'] ?></span>
                                <?php
                            }
                        ?>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
</body>
</html>