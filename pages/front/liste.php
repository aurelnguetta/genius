<?php require_once('../php/mysqli.php') ?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SCHOOL BOX</title>
    <link rel="stylesheet" href="../../css/form.css">
</head>
<body>
    <div class="back">
        <section>
            <nav class="menu">
                <ul>
                    <li><a href="../../index.php">AJOUTER ETUDIANT</a></li>
                    <li class="active"><a href="#">LISTE DES ETUDIANTS</a></li>
                    <!-- <li><a href="#">MA CARTE ETUDIANT</a></li> -->
                </ul>
            </nav>
            <div class="liste">
                <!-- <h3>LISTE DES ETUDIANTS</h3> -->
                <table>
                    <thead>
                        <tr>
                            <th>Matricule</th>
                            <th>Nom</th>
                            <th>Prénoms</th>
                            <th>Date de Naiss.</th>
                            <th>Lieu de Naiss.</th>
                            <th>Sexe</th>
                            <th colspan="4">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                          $query = "SELECT * FROM students ORDER BY matricule";

                            $result = $connexion->query($query);

                            while ($row = $result->fetch_assoc()) {
                                ?>
                            <tr>
                                <td><?php echo $row['matricule'] ?></td>
                                <td><?php echo $row['nom'] ?></td>
                                <td><?php echo $row['prenom'] ?></td>
                                <td><?php echo $row['datenaiss'] ?></td>
                                <td><?php echo $row['lieunaiss'] ?></td>
                                <td><?php echo $row['sexe'] ?></td>
                                <td><a href="cart.php?matricule=<?php echo $row['matricule'] ?>"><img src="../../images/cart.jpg" alt=""></a></td>
                            </tr> <?php
                            
                            } mysqli_close($connexion);?>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</body>
</html>