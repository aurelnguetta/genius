-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 16, 2021 at 07:47 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `schoolbox`
--
CREATE DATABASE schoolbox;
-- --------------------------------------------------------
--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `matricule` varchar(30) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `datenaiss` datetime NOT NULL,
  `lieunaiss` varchar(80) NOT NULL,
  `sexe` varchar(2) NOT NULL,
  `classe` varchar(50) DEFAULT NULL,
  `profil` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`matricule`, `nom`, `prenom`, `datenaiss`, `lieunaiss`, `sexe`, `classe`, `profil`) VALUES
('21-ESATIC0338AB', 'N\'GUETTA', 'MARC-AUREL', '1995-01-15 00:00:00', 'ABIDJAN', 'M', 'LICENCE 3 - DASI', NULL),
('21-ESATIC03580TA', 'YAO', 'AKA GUY', '1992-10-01 00:00:00', 'BONOUA', 'M', NULL, NULL),
('21-ESATIC04GK', 'SEKRE', 'KOUAKOU FULGENCE', '2000-11-10 00:00:00', 'BONDOUKOU', 'M', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`matricule`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
