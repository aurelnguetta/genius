<?php require_once('pages/php/connexion.php') ?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SCHOOL BOX</title>
    <link rel="stylesheet" href="css/form.css">
</head>
<body>
    <div class="back">
        <section>
            <nav class="menu">
                <ul>
                    <li class="active"><a href="#">AJOUTER ETUDIANT</a></li>
                    <li><a href="pages/front/liste.php">LISTE DES ETUDIANTS</a></li>
                    <!-- <li><a href="#">MA CARTE ETUDIANT</a></li> -->
                </ul>
            </nav>
            <form action="pages/php/newStudent.php" method="post">
                <h3>AJOUTER UN ETUDIANT</h3>
                <label for="matricule">Matricule</label>
                <input type="text" name="matricule" id="matricule" placeholder="Matricule">
    
                <label for="nom">Nom</label>
                <input type="text" name="nom" id="nom" placeholder="Nom">
    
                <label for="prenom">Prenoms</label>
                <input type="text" name="prenom" id="prenom" placeholder="Prénoms">
    
                <label for="datenaiss">Date de Naissance</label>
                <input type="date" name="datenaiss" id="datenaiss">
    
                <label for="lieunaiss">Lieu de Naissance</label>
                <select name="lieunaiss" id="lieunaiss">
                    <option>Lieu de naissance</option>
                    <?php include_once('pages/php/villes.php') ?>
                    
                </select>
    
                <label for="sexe">Sexe</label>
                <select name="sexe" id="sexe">
                    <option>Sexe</option>
                    <option value="F">Féminin</option>
                    <option value="M">Masculin</option>
                </select>
    
                <input type="submit" name="btnEnregistrer" value="Enregistrer">
    
            </form>
            <div class="img flex justify-center align-center">
                <img class="sm-img" src="images/user.png" alt="">
            </div>
        </section>
    </div>
</body>
</html>